import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class App {

	
	public static void main(String [] args) throws InterruptedException{
	
		long intervalStart = 10;
		long intervalEnd = 20;
		
		DivisorCounterTaskResult result = new DivisorCounterTaskResult();
		ExecutorService executor =Executors.newFixedThreadPool(10);
		
		for (long zahl = intervalStart; zahl <= intervalEnd; ++zahl){
			DivisorCounterTask task = new DivisorCounterTask(result, zahl);
			executor.execute(task);
		}
		
		//executor.shutdown();
		
		executor.awaitTermination(10, TimeUnit.SECONDS);
		
		
		System.out.println(result.getAktuelleZahl() + "/" + result.getaktuelleAnzahlDivisoren());
	}
}


